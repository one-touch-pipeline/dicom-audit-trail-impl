# dicom-audit-trail-impl

An implementation layer for the DICOM Audit Trail project, adding the required templates and factory methods for common OTP Audit events.

## License

This project is licensed under the [MIT license](LICENSE).

### ... of your contributions

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in dicom-audit-trail-impl by you, shall be licensed as MIT, without any additional
terms or conditions.
