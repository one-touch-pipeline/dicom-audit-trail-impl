/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.impl;

import de.dkfz.odcf.audit.impl.enums.DicomCode.*;
import de.dkfz.odcf.audit.xml.layer.EventIdentification.EventActionCode;
import java.nio.file.*;
import java.time.*;
import java.util.*;
import org.junit.*;

import static de.dkfz.odcf.audit.impl.DicomAuditLogger.log;
import static de.dkfz.odcf.audit.impl.OtpDicomAuditFactory.*;
import static de.dkfz.odcf.audit.xml.layer.EventIdentification.EventOutcomeIndicator.*;

public class OtpDicomAuditFactoryTest {

    @Before
    public void splitTests() {
        System.out.println("--------------------------------------");
    }
    
    @Test
    public void testGetActorStart() {
        System.out.println(getActorStart(SUCCESS, Instant.now(), "f.tichawa@dkfz.de").toXmlString());
    }

    @Test
    public void testGetActorStop() {
        System.out.println(getActorStop(SUCCESS, Instant.now(), "f.tichawa@dkfz.de").toXmlString());
    }

    @Test
    public void testLogging() {
        log(getActorStart(SUCCESS, Instant.now(), "f.tichawa@dkfz.de"));
    }
    
    @Test
    public void testGetAuditLogUse() {
        System.out.println(getAuditLogUse(Instant.now(), "f.tichawa@dkfz.de", Paths.get("AuditLog").toUri()).toXmlString());
    }

    @Test
    public void testGetUserLogin() {
        System.out.println(getUserLogin(SUCCESS, Instant.now(), "f.tichawa@dkfz.de").toXmlString());
    }

    @Test
    public void testGetUserLogout() {
        System.out.println(getUserLogout(SUCCESS, Instant.now(), "f.tichawa@dkfz.de").toXmlString());
    }

    @Test
    public void testGetEmergencyOverrideStart() {
        System.out.println(getEmergencyOverrideStart(SUCCESS, Instant.now(), "f.tichawa@dkfz.de").toXmlString());
    }
    
    @Test
    public void testGetEmergencyOverrideStop() {
        System.out.println(getEmergencyOverrideStop(SUCCESS, Instant.now(), "f.tichawa@dkfz.de").toXmlString());
    }
    
    @Test
    public void testGetAuditRecordingStart() {
        System.out.println(getAuditRecordingStart(SUCCESS, Instant.now(), "f.tichawa@dkfz.de").toXmlString());
    }
    
    @Test
    public void testGetAuditRecordingStop() {
        System.out.println(getAuditRecordingStop(SUCCESS, Instant.now(), "f.tichawa@dkfz.de").toXmlString());
    }
    
    @Test
    public void testGetRestrictedFunctionUsed() {
        System.out.println(getRestrictedFunctionUsed(SUCCESS, Instant.now(), "f.tichawa@dkfz.de", "/path/to/resource").toXmlString());
    }

    @Test
    public void testGetPatientRecordRead() {
        System.out.println(getPatientRecordRead(SUCCESS, Instant.now(), "f.tichawa@dkfz.de", "f.tichawa@dkfz.de", "ANONYMIZED", true, false).toXmlString());
    }

    @Test
    public void testGetPatientRecordCreate() {
        System.out.println(getPatientRecordCreate(SUCCESS, Instant.now(), "f.tichawa@dkfz.de", "f.tichawa@dkfz.de", "ANONYMIZED", true, false).toXmlString());
    }

    @Test
    public void testGetPatientRecordUpdate() {
        System.out.println(getPatientRecordUpdate(SUCCESS, Instant.now(), "f.tichawa@dkfz.de", "f.tichawa@dkfz.de", "ANONYMIZED", true, false, Arrays.asList("Update 1", "Update 2")).toXmlString());
    }

    @Test
    public void testGetPatientRecordEvent() {
        System.out.println(getPatientRecordEvent(SUCCESS, Instant.now(), "f.tichawa@dkfz.de", EventActionCode.EXECUTE, "f.tichawa@dkfz.de", "ANONYMIZED", true, false, Arrays.asList("Update 1", "Update 2")).toXmlString());
    }

    @Test
    public void testGetStudyDelete() {
        System.out.println(getStudyDelete(SUCCESS, Instant.now(), "f.tichawa@dkfz.de", "f.tichawa@dkfz.de", "ANONYMIZED", true, false, Arrays.asList("Study 1", "Study 2")).toXmlString());
    }

    @Test
    public void testGetPermissionRevoked() {
        System.out.println(getPermissionRevoked(SUCCESS, Instant.now(), "f.tichawa@dkfz.de", "dummy@dummy", "882", OtpPermissionCode.FILE_ACCESS, OtpPermissionCode.MANAGE_USERS).toXmlString());
    }

    @Test
    public void testGetPermissionGranted() {
        System.out.println(getPermissionGranted(SUCCESS, Instant.now(), "f.tichawa@dkfz.de", "dummy@dummy", "882", OtpPermissionCode.DELEGATE_MANAGE_USERS).toXmlString());
    }

    @Test
    public void testGetUserSwitched() {
        System.out.println(getUserSwitched(SUCCESS, Instant.now(), "f.tichawa@dkfz.de", "dummy@dummy"));
    }

    @Test
    public void testGetUserActivated() {
        System.out.println(getUserActivated(SUCCESS, Instant.now(), "f.tichawa@dkfz.de", "dummy@dummy", "Project 7"));
    }

    @Test
    public void testGetUserDectivated() {
        System.out.println(getUserDeactivated(SUCCESS, Instant.now(), "f.tichawa@dkfz.de", "dummy@dummy", "Project 7"));
    }
}
