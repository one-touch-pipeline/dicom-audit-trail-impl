/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.impl;

import de.dkfz.odcf.audit.impl.enums.DicomCode.*;
import de.dkfz.odcf.audit.xml.layer.*;
import de.dkfz.odcf.audit.xml.layer.EventIdentification.EventOutcomeIndicator;
import java.net.*;
import java.time.*;
import java.util.*;
import org.slf4j.*;
import org.slf4j.event.*;

import static de.dkfz.odcf.audit.impl.OtpDicomAuditFactory.*;

/**
 * A simplified API for the OtpDicomAuditFactory.<br>
 * The fully automated logging methods offered by this class should be preferred
 * over manually creating and logging AuditMessage object via
 * OtpDicomAuditFactory wherever possible.
 *
 * @author Florian Tichawa
 */
public class DicomAuditLogger {

    private static Level LOG_LEVEL = Level.INFO;
    private static String LOG_NAME = null;
    private static Logger LOGGER;

    private DicomAuditLogger() {
    }

    /**
     * Overrides the default logger settings.
     *
     * @param logName Reference to the new logger
     * @param logLevel Log level for future Audit events
     */
    public static void setLogger(String logName, Level logLevel) {
        LOG_NAME = logName;
        LOG_LEVEL = logLevel;
        LOGGER = LoggerFactory.getLogger(LOG_NAME);
    }

    /**
     * Low level API method to log custom AuditMessages to the default
     * DicomAuditLogger.
     *
     * @param msg AuditMessage to log
     */
    public static void log(AuditMessage msg) {
        if (LOGGER == null) {
            LOGGER = LoggerFactory.getLogger(DicomAuditLogger.class);
        }
        switch (LOG_LEVEL) {
            case TRACE:
                LOGGER.trace("{}", msg);
                break;
            case DEBUG:
                LOGGER.debug("{}", msg);
                break;
            case INFO:
                LOGGER.info("{}", msg);
                break;
            case WARN:
                LOGGER.warn("{}", msg);
                break;
            case ERROR:
                LOGGER.error("{}", msg);
                break;
        }
    }

    /**
     * Creates and logs an Actor Start event (DCM 110120) with Instant.now() as
     * default timestamp representing the start of an OTP server.
     *
     * @param outcome EventOutcomeIndicator
     * @param UID User ID for ActiveParticipant requesting the Actor Start event
     */
    public static void logActorStart(EventOutcomeIndicator outcome, String UID) {
        log(getActorStart(outcome, Instant.now(), UID));
    }

    /**
     * Creates and logs an Actor Stop event (DCM 110121) with Instant.now() as
     * default timestamp representing the start of an OTP server.
     *
     * @param outcome EventOutcomeIndicator
     * @param UID User ID for ActiveParticipant requesting the Actor Stop event
     */
    public static void logActorStop(EventOutcomeIndicator outcome, String UID) {
        log(getActorStop(outcome, Instant.now(), UID));
    }

    /**
     * Creates and logs an Audit Log Used event (DCM 110101) with Instant.now()
     * as default timestamp representing the successful access to the given
     * audit log.
     *
     * @param UID ID of the user accessing the audit log
     * @param auditLog URI referencing the accessed audit log
     */
    public static void logAuditLogUsed(String UID, URI auditLog) {
        log(getAuditLogUse(Instant.now(), UID, auditLog));
    }

    /**
     * Creates and logs an User Login event (DCM 110122) with Instant.now() as
     * default timestamp representing a login attempt (Successful or failed)
     * into the OTP web server.
     *
     * @param outcome SUCCESS for successful logins, or (Usually) MINOR_FAILURE
     * for failed logins
     * @param UID ID of the user trying to log in
     */
    public static void logUserLogin(EventOutcomeIndicator outcome, String UID) {
        log(getUserLogin(outcome, Instant.now(), UID));
    }

    /**
     * Creates and logs an User Logout event (DCM 110123) with Instant.now() as
     * default timestamp representing the active logout from OTP.<br>
     * Do not use this method for automatic logout processes due to inactivity.
     *
     * @param outcome Usually SUCCESS
     * @param UID ID of the user logging out
     */
    public static void logUserLogout(EventOutcomeIndicator outcome, String UID) {
        log(getUserLogout(outcome, Instant.now(), UID));
    }

    /**
     * Creates and logs an Emergency Override Started event (DCM 110127) with
     * Instant.now() as default timestamp representing an attempt to enter Super
     * User mode in OTP.
     *
     * @param outcome EventOutcomeIndicator
     * @param UID ID of the user attempting to enter Super User mode
     */
    public static void logEmergencyOverrideStart(EventOutcomeIndicator outcome, String UID) {
        log(getEmergencyOverrideStart(outcome, Instant.now(), UID));
    }

    /**
     * Creates and logs an Emergency Override Started event (DCM 110138) with
     * Instant.now() as default timestamp representing an attempt to leave Super
     * User mode in OTP.
     *
     * @param outcome Usually SUCCESS
     * @param UID ID of the user leaving Super User mode
     */
    public static void logEmergencyOverrideStop(EventOutcomeIndicator outcome, String UID) {
        log(getEmergencyOverrideStop(outcome, Instant.now(), UID));
    }

    /**
     * Creates and logs an Audit Recording Started event (DCM 110134) with
     * Instant.now() as default timestamp representing an attempt to start the
     * Audit Trail in OTP.
     *
     * @param outcome EventOutcomeIndicator
     * @param UID ID of the user attempting to start audit recording
     */
    public static void logAuditRecordingStart(EventOutcomeIndicator outcome, String UID) {
        log(getAuditRecordingStart(outcome, Instant.now(), UID));
    }

    /**
     * Creates and logs an Audit Recording Stopped event (DCM 110133) with
     * Instant.now() as default timestamp representing an attempt to stop the
     * Audit Trail in OTP.
     *
     * @param outcome EventOutcomeIndicator
     * @param UID ID of the user attempting to stop audit recording
     */
    public static void logAuditRecordingStop(EventOutcomeIndicator outcome, String UID) {
        log(getAuditRecordingStop(outcome, Instant.now(), UID));
    }

    /**
     * Creates and logs an Use of Restricted Function event (DCM 110132) with
     * Instant.now() as default timestamp representing an attempt to use a
     * function with restricted access in OTP. It is not recommended to use this
     * function for successful cases, as this may result in far too many audit
     * logs.
     *
     * @param outcome EventOutcomeIndicator
     * @param UID ID of the user attempting to use the restricted function
     * @param resourceID ID of the resource containing the restricted function
     */
    public static void logRestrictedFunctionUsed(EventOutcomeIndicator outcome, String UID, String resourceID) {
        log(getRestrictedFunctionUsed(outcome, Instant.now(), UID, resourceID));
    }

    /**
     * Creates and logs a Patient Record event (DCM 110113) with Instant.now()
     * as default timestamp and anonymized, non-encrypted patient data
     * representing a read operation on a single patient record.
     *
     * @param outcome EventOutComeIndicator
     * @param UID ID of the user attempting to read the patient data
     * @param PID ID of the patient being read
     */
    public static void logPatientRecordRead(EventOutcomeIndicator outcome, String UID, String PID) {
        log(getPatientRecordRead(outcome, Instant.now(), UID, PID, "ANONYMIZED", true, false));
    }

    /**
     * Creates and logs a Patient Record event (DCM 110113) with Instant.now()
     * as default timestamp and anonymized, non-encrypted patient data
     * representing a create operation for a single patient record.
     *
     * @param outcome EventOutComeIndicator
     * @param UID ID of the user attempting to create the patient data
     * @param PID ID of the patient being created
     */
    public static void logPatientRecordCreate(EventOutcomeIndicator outcome, String UID, String PID) {
        log(getPatientRecordCreate(outcome, Instant.now(), UID, PID, "ANONYMIZED", true, false));
    }

    /**
     * Creates and logs a Patient Record event (DCM 110113) with Instant.now()
     * as default timestamp and anonymized, non-encrypted patient data
     * representing an update operation on a single patient record.
     *
     * @param outcome EventOutComeIndicator
     * @param UID ID of the user attempting to update the patient data
     * @param PID ID of the patient being updated
     * @param operations List of update operations being made
     */
    public static void logPatientRecordUpdate(EventOutcomeIndicator outcome, String UID, String PID, Collection<String> operations) {
        log(getPatientRecordUpdate(outcome, Instant.now(), UID, PID, "ANONYMIZED", true, false, operations));
    }

    /**
     * Creates and logs a DICOM Study Deleted event (DCM 110105) with
     * Instant.now() as default timestamp and anonymized, non-encrypted patient
     * data representing the deletion of one or more studies related to the same
     * patient.
     *
     * @param outcome EventOutComeIndicator
     * @param UID ID of the user attempting to delete the studies
     * @param PID ID of the patient belonging to the deleted studies
     * @param SIDs List of all studies being deleted
     */
    public static void logStudyDelete(EventOutcomeIndicator outcome, String UID, String PID, Collection<String> SIDs) {
        log(getStudyDelete(outcome, Instant.now(), UID, PID, "ANONYMIZED", true, false, SIDs));
    }

    /**
     * Creates and logs a Permission Granted event (99DCMOTP 3) with
     * Instant.now() as default timestamp representing an attempt to modify the
     * access permissions for one user to a specific project in OTP.
     *
     * @param outcome EventOutcomeIndicator
     * @param requestorUserId User ID for ActiveParticipant requesting
     * permission change
     * @param targetUserId User ID for the user being modified
     * @param studyUID UID of the study being modified
     * @param permissions Permission(s) to add / remove from the user being
     * modified
     */
    public static void logPermissionGranted(EventOutcomeIndicator outcome, String requestorUserId, String targetUserId, String studyUID, OtpPermissionCode... permissions) {
        log(getPermissionGranted(outcome, Instant.now(), requestorUserId, targetUserId, studyUID, permissions));
    }

    /**
     * Creates and logs a Permission Revoked event (99DCMOTP 4) with
     * Instant.now() as default timestamp representing an attempt to modify the
     * access permissions for one user to a specific project in OTP.
     *
     * @param outcome EventOutcomeIndicator
     * @param requestorUserId User ID for user requesting the permission change
     * @param targetUserId User ID for the user being modified
     * @param studyUID UID of the study being modified
     * @param permissions Permission(s) to add / remove from the user being
     * modified
     */
    public static void logPermissionRevoked(EventOutcomeIndicator outcome, String requestorUserId, String targetUserId, String studyUID, OtpPermissionCode... permissions) {
        log(getPermissionRevoked(outcome, Instant.now(), requestorUserId, targetUserId, studyUID, permissions));
    }

    /**
     * Creates and logs a User Activated event (99DCMOTP 1) with Instant.now()
     * as default timestamp.
     *
     * @param outcome EventOutcomeIndicator
     * @param requestorUserId User ID for the user requesting the activation
     * @param targetUserId User ID for the user being activated
     * @param studyUID UID of the study being modified
     */
    public static void logUserActivated(EventOutcomeIndicator outcome, String requestorUserId, String targetUserId, String studyUID) {
        log(getUserActivated(outcome, Instant.now(), requestorUserId, targetUserId, studyUID));
    }

    /**
     * Creates a User Deactivated event (99DCMOTP 2) with Instant.now() as
     * default timestamp.
     *
     * @param outcome EventOutcomeIndicator
     * @param requestorUserId User ID for the user requesting the deactivation
     * @param targetUserId User ID for the user being deactivated
     * @param studyUID UID of the study being modified
     */
    public static void logUserDeactivated(EventOutcomeIndicator outcome, String requestorUserId, String targetUserId, String studyUID) {
        log(getUserDeactivated(outcome, Instant.now(), requestorUserId, targetUserId, studyUID));
    }

    /**
     * Creates and logs a User Switched event (99DCMOTP 0) with Instant.now() as
     * default timestamp representing an attempt to impersonate another user in
     * the Spring Security framework.
     *
     * @param outcome EventOutcomeIndicator
     * @param requestorUserId User ID for the user impersonating another user
     * @param targetUserId User ID for the user being impersonated
     */
    public static void logUserSwitched(EventOutcomeIndicator outcome, String requestorUserId, String targetUserId) {
        log(getUserSwitched(outcome, Instant.now(), requestorUserId, targetUserId));
    }
}
