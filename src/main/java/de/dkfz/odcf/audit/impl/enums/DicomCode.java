/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.impl.enums;

public abstract class DicomCode {

    public static final String OTP_CSN = "99DCMOTP";

    private final int codeValue;
    private final String description;
    private final String codeSystemName;

    public DicomCode(String codeSystemName, int codeValue, String description) {
        this.codeSystemName = codeSystemName;
        this.codeValue = codeValue;
        this.description = description;
    }

    /**
     * Returns the code value as defined in the corresponding dictionary.
     *
     * @return DICOM code value
     */
    public int getCodeValue() {
        return codeValue;
    }

    /**
     * Returns the code meaning as defined in the corresponding dictionary.
     *
     * @return DICOM code meaning
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the identifier of the corresponding dictionary. Custom
     * dictionaries should always start with "99DCM".
     *
     * @return Identifier of the DICOM dictionary
     */
    public String getCodeSystemName() {
        return codeSystemName;
    }

    /**
     * Returns the triplet string representation as used in the DICOM
     * specification: '(&lt;CSN&gt;, &lt;Code&gt;, &lt;Default Code
     * Meaning&gt;)'.
     *
     * @return Triplet string representation of this DICOM code
     */
    @Override
    public String toString() {
        return '(' + getCodeSystemName() + ", " + getCodeValue() + ", " + getDescription() + ')';
    }

    /**
     * Custom event codes in the 99DCMOTP name space.
     */
    public static class OtpEventCode extends DicomCode {

        public static final OtpEventCode USER_SWITCH = new OtpEventCode(0, "OTP User Switch event");
        public static final OtpEventCode USER_ACTIVATED = new OtpEventCode(1, "OTP User Activation event");
        public static final OtpEventCode USER_DEACTIVATED = new OtpEventCode(2, "OTP User Deactivation event");
        public static final OtpEventCode PERMISSION_GRANTED = new OtpEventCode(3, "OTP User Permission Granted event");
        public static final OtpEventCode PERMISSION_REVOKED = new OtpEventCode(4, "OTP User Permission Revoked event");

        private OtpEventCode(int codeValue, String description) {
            super(OTP_CSN, codeValue, description);
        }
    }

    /**
     * Custom ParticipantObjectID codes in the 99DCMOTP name space.
     */
    public static class OtpUserRoleCode extends DicomCode {

        public static final OtpUserRoleCode ADMIN = new OtpUserRoleCode(100, "OTP Admin role");
        public static final OtpUserRoleCode OPERATOR = new OtpUserRoleCode(101, "OTP Operator role");
        public static final OtpUserRoleCode USER_SWITCH = new OtpUserRoleCode(102, "OTP User Switch role");

        private OtpUserRoleCode(int codeValue, String description) {
            super(OTP_CSN, codeValue, description);
        }
    }

    /**
     * Custom ParticipantObjectID codes in the 99DCMOTP name space.
     */
    public static class OtpPermissionCode extends DicomCode {

        public static final OtpPermissionCode FILE_ACCESS = new OtpPermissionCode(1000, "OTP File Access permission");
        public static final OtpPermissionCode MANAGE_USERS = new OtpPermissionCode(1001, "OTP Manage Users permission");
        public static final OtpPermissionCode DELEGATE_MANAGE_USERS = new OtpPermissionCode(1002, "OTP Delegate Manage Users permission");
        public static final OtpPermissionCode OTP_ACCESS = new OtpPermissionCode(1003, "OTP Access permission");

        private OtpPermissionCode(int codeValue, String description) {
            super(OTP_CSN, codeValue, description);
        }
    }

    /**
     * Custom ParticipantObjectType codes in the 99DCMOTP name space.
     */
    public static class OtpObjectTypeCode extends DicomCode {

        public static final OtpObjectTypeCode PERMISSION = new OtpObjectTypeCode(2000, "Permission");

        private OtpObjectTypeCode(int codeValue, String description) {
            super(OTP_CSN, codeValue, description);
        }
    }
}
