/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.impl;

import de.dkfz.odcf.audit.*;
import de.dkfz.odcf.audit.impl.enums.DicomCode.*;
import de.dkfz.odcf.audit.xml.*;
import de.dkfz.odcf.audit.xml.layer.*;
import de.dkfz.odcf.audit.xml.layer.EventIdentification.*;
import de.dkfz.odcf.audit.xml.layer.ParticipantObjectIdentification.*;
import java.net.*;
import java.time.*;
import java.util.*;

/**
 * An implementation of the de.dkfz.odcf.audit.AuditFactory designed for usage
 * on OTP server.<br>
 * This class is a low level API and should only be used when the options in
 * AuditLogger do not suffice.
 *
 * @author Florian Tichawa
 */
public class OtpDicomAuditFactory {

    private static final char UID_SEPARATOR = '.';
    private static final String UID_PREFIX = "UID.temp";

    public enum UniqueIdentifierType {
        OTP_INSTANCE(0), STUDY(1), SOP(2), SOP_INSTANCE(3);

        private final int intVal;

        UniqueIdentifierType(int intVal) {
            this.intVal = intVal;
        }

        public int getIntVal() {
            return intVal;
        }
    }

    private OtpDicomAuditFactory() {
    }

    /**
     * Creates an Actor Start event (DCM 110120) from the template library.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID User ID for ActiveParticipant requesting the Actor Start event
     * @return A loggable AuditMessage representing an OTP Start Event
     */
    public static AuditMessage getActorStart(EventOutcomeIndicator outcome, Instant dateTime, String UID) {
        return fillActorStartStop(AuditFactory.ofType("ActorStart"), outcome, dateTime, UID);
    }

    /**
     * Creates an Actor Stop event (DCM 110121) from the template library.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID User ID for ActiveParticipant requesting the Actor Stop event
     * @return A loggable AuditMessage representing an OTP Stop Event
     */
    public static AuditMessage getActorStop(EventOutcomeIndicator outcome, Instant dateTime, String UID) {
        return fillActorStartStop(AuditFactory.ofType("ActorStop"), outcome, dateTime, UID);
    }

    /**
     * Creates an Audit Log Used event (DCM 110101) from the template library.
     *
     * @param dateTime Event timestamp
     * @param UID ID of the user attempting to access the audit log
     * @param auditLog URI referencing the accessed audit log
     * @return A loggable AuditMessage representing an Audit Log Used event
     */
    public static AuditMessage getAuditLogUse(Instant dateTime, String UID, URI auditLog) {
        AuditMessage msg = AuditFactory.ofType("AuditLogUsed")
                .withParticipant(new ActiveParticipant()
                        .withUserId(UID)
                        .asRequestor())
                .withAttr(Selector.parse("ParticipantObjectIdentification[ParticipantObjectTypeCodeRole=13]"), "ParticipantObjectID", auditLog);
        msg.getEventId().withDateTime(dateTime);
        return msg;
    }

    /**
     * Creates an User Login event (DCM 110122) from the template library.
     *
     * @param outcome SUCCESS for successful logins, or (Usually) MINOR_FAILURE
     * for failed logins
     * @param dateTime Event timestamp
     * @param UID ID of the user trying to log in
     * @return A loggable AuditMessage representing an User Login event
     */
    public static AuditMessage getUserLogin(EventOutcomeIndicator outcome, Instant dateTime, String UID) {
        return fillDefault(AuditFactory.ofType("UserLogin"), outcome, dateTime, UID);
    }

    /**
     * Creates an User Logout event (DCM 110122) from the template library.<br>
     * Do not use this method for automatic logout processes due to inactivity.
     *
     * @param outcome Usually SUCCESS
     * @param dateTime Event timestamp
     * @param UID ID of the user logging out
     * @return A loggable AuditMessage representing an User Logout event
     */
    public static AuditMessage getUserLogout(EventOutcomeIndicator outcome, Instant dateTime, String UID) {
        return fillDefault(AuditFactory.ofType("UserLogout"), outcome, dateTime, UID);
    }

    /**
     * Creates an Emergency Override Started event (DCM 110127) from the
     * template library.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID ID of the user attempting to start an Emergency Override
     * @return A loggable AuditMessage representing an Emergency Override
     * Started event
     */
    public static AuditMessage getEmergencyOverrideStart(EventOutcomeIndicator outcome, Instant dateTime, String UID) {
        return fillDefault(AuditFactory.ofType("EmergencyOverrideStart"), outcome, dateTime, UID);
    }

    /**
     * Creates an Emergency Override Stopped event (DCM 110138) from the
     * template library.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID ID of the user attempting to end an Emergency Override
     * @return A loggable AuditMessage representing an Emergency Override
     * Stopped event
     */
    public static AuditMessage getEmergencyOverrideStop(EventOutcomeIndicator outcome, Instant dateTime, String UID) {
        return fillDefault(AuditFactory.ofType("EmergencyOverrideStop"), outcome, dateTime, UID);
    }

    /**
     * Creates an Audit Recording Started event (DCM 110134) from the template
     * library.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID ID of the user attempting to start audit recording
     * @return A loggable AuditMessage representing an Audit Recording Started
     * event
     */
    public static AuditMessage getAuditRecordingStart(EventOutcomeIndicator outcome, Instant dateTime, String UID) {
        return fillDefault(AuditFactory.ofType("AuditRecordingStart"), outcome, dateTime, UID);
    }

    /**
     * Creates an Audit Recording Stopped event (DCM 110133) from the template
     * library.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID ID of the user attempting to stop audit recording
     * @return A loggable AuditMessage representing an Audit Recording Stopped
     * event
     */
    public static AuditMessage getAuditRecordingStop(EventOutcomeIndicator outcome, Instant dateTime, String UID) {
        return fillDefault(AuditFactory.ofType("AuditRecordingStop"), outcome, dateTime, UID);
    }

    /**
     * Creates an Use of Restricted Function event (DCM 110132) from the
     * template library. It is not recommended to use this function for
     * successful cases, as this may result in far too many audit logs.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID ID of the user attempting to use restricted function
     * @param resourceID ID of the resource containing the restricted function
     * @return A loggable AuditMessage representing an Use of Restricted
     * Function event
     */
    public static AuditMessage getRestrictedFunctionUsed(EventOutcomeIndicator outcome, Instant dateTime, String UID, String resourceID) {
        return fillDefault(AuditFactory.ofType("RestrictedFunctionUsed"), outcome, dateTime, UID)
                .withObject(new ParticipantObjectIdentification()
                        .withId(resourceID)
                        .withName(resourceID)
                        .withTypeCodeRole(ObjectTypeCode.SYSTEM_OBJECT, ObjectTypeCodeRole.SECURITY_RESOURCE)
                        .withIdTypeCode(ObjectIdTypeCode.URI));
    }

    /**
     * Creates a Permission Granted event (99DCMOTP 3) from the template
     * library.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param requestorUserId User ID for ActiveParticipant requesting
     * permission change
     * @param targetUserId User ID for the user being modified
     * @param studyUID UID of the study being modified
     * @param permissions Permission(s) to add / remove from the user being
     * modified
     * @return A loggable AuditMessage representing a Permission Revoked event
     */
    public static AuditMessage getPermissionGranted(EventOutcomeIndicator outcome, Instant dateTime, String requestorUserId, String targetUserId, String studyUID, OtpPermissionCode... permissions) {
        return fillPermission(AuditFactory.ofType("PermissionGranted"), outcome, dateTime, requestorUserId, targetUserId, studyUID, permissions);
    }

    /**
     * Creates a Permission Revoked event (99DCMOTP 4) from the template
     * library.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param requestorUserId User ID for the user requesting permission change
     * @param targetUserId User ID for the user being modified
     * @param studyUID UID of the study being modified
     * @param permissions Permission(s) to add / remove from the user being
     * modified
     * @return A loggable AuditMessage representing a Permission Revoked event
     */
    public static AuditMessage getPermissionRevoked(EventOutcomeIndicator outcome, Instant dateTime, String requestorUserId, String targetUserId, String studyUID, OtpPermissionCode... permissions) {
        return fillPermission(AuditFactory.ofType("PermissionRevoked"), outcome, dateTime, requestorUserId, targetUserId, studyUID, permissions);
    }

    /**
     * Creates a User Switched event (99DCMOTP 0) from the template library.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param requestorUserId User ID for the user impersonating another user
     * @param targetUserId User ID for the user being impersonated
     * @return A loggable AuditMessage representing a User Switched event
     */
    public static AuditMessage getUserSwitched(EventOutcomeIndicator outcome, Instant dateTime, String requestorUserId, String targetUserId) {
        return fillTargetUser(AuditFactory.ofType("UserSwitched"), outcome, dateTime, requestorUserId, targetUserId);
    }

    /**
     * Creates a User Activated event (99DCMOTP 1) from the template library.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param requestorUserId User ID for the user requesting the activation
     * @param targetUserId User ID for the user being activated
     * @param studyUID UID of the study being modified
     * @return A loggable AuditMessage representing a User Activated event
     */
    public static AuditMessage getUserActivated(EventOutcomeIndicator outcome, Instant dateTime, String requestorUserId, String targetUserId, String studyUID) {
        return fillUserActivatedDeactivated(AuditFactory.ofType("UserActivated"), outcome, dateTime, requestorUserId, targetUserId, studyUID);
    }

    /**
     * Creates a User Deactivated event (99DCMOTP 2) from the template library.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param requestorUserId User ID for the user requesting the deactivation
     * @param targetUserId User ID for the user being deactivated
     * @param studyUID UID of the study being modified
     * @return A loggable AuditMessage representing a User Deactivated event
     */
    public static AuditMessage getUserDeactivated(EventOutcomeIndicator outcome, Instant dateTime, String requestorUserId, String targetUserId, String studyUID) {
        return fillUserActivatedDeactivated(AuditFactory.ofType("UserDeactivated"), outcome, dateTime, requestorUserId, targetUserId, studyUID);
    }

    /**
     * Creates a Patient Record event (DCM 110113) representing a read operation
     * on a single patient record.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID ID of the user attempting to perform the operation
     * @param PID ID of the patient being read
     * @param patientName Human readable name of the patient being read
     * @param anonymized Whether the patient data is anonymized
     * @param encrypted Whether the patient data is encrypted
     * @return A loggable AuditMessage representing a Patient Record Read event
     * @deprecated Marked as Deprecated due to missing implementation of the
     * operations field.
     */
    public static AuditMessage getPatientRecordRead(EventOutcomeIndicator outcome, Instant dateTime, String UID, String PID, String patientName, boolean anonymized, boolean encrypted) {
        return getPatientRecordEvent(outcome, dateTime, UID, EventActionCode.READ, PID, patientName, anonymized, encrypted, new LinkedList<>());
    }

    /**
     * Creates a Patient Record event (DCM 110113) representing a create
     * operation for a single patient record.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID ID of the user attempting to perform the operation
     * @param PID ID of the patient being created
     * @param patientName Human readable name of the patient being created
     * @param anonymized Whether the patient data is anonymized
     * @param encrypted Whether the patient data is encrypted
     * @return A loggable AuditMessage representing a Patient Record Created
     * event
     * @deprecated Marked as Deprecated due to missing implementation of the
     * operations field.
     */
    public static AuditMessage getPatientRecordCreate(EventOutcomeIndicator outcome, Instant dateTime, String UID, String PID, String patientName, boolean anonymized, boolean encrypted) {
        return getPatientRecordEvent(outcome, dateTime, UID, EventActionCode.CREATE, PID, patientName, anonymized, encrypted, new LinkedList<>());
    }

    /**
     * Creates a Patient Record event (DCM 110113) representing an update
     * operation on a single patient record.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID ID of the user attempting to perform the operation
     * @param PID ID of the patient being created
     * @param patientName Human readable name of the patient being updated
     * @param anonymized Whether the patient data is anonymized
     * @param encrypted Whether the patient data is encrypted
     * @param operations List of update operations being made
     * @return A loggable AuditMessage representing a Patient Record Updated
     * @deprecated Marked as Deprecated due to missing implementation of the
     * operations field.
     */
    public static AuditMessage getPatientRecordUpdate(EventOutcomeIndicator outcome, Instant dateTime, String UID, String PID, String patientName, boolean anonymized, boolean encrypted, Collection<String> operations) {
        return getPatientRecordEvent(outcome, dateTime, UID, EventActionCode.UPDATE, PID, patientName, anonymized, encrypted, operations);
    }

    /**
     * Creates a Patient Record event (DCM 110113) representing an operation on
     * a single patient record.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID ID of the user attempting to perform the operation
     * @param actionCode Event action type (READ, CREATE, UPDATE, DELETE)
     * @param PID ID of the patient being updated
     * @param patientName Human readable name of the patient being updated
     * @param anonymized Whether the patient data is anonymized
     * @param encrypted Whether the patient data is encrypted
     * @param operations List of operations performed on the patient data
     * @return A loggable AuditMessage representing a Patient Record event
     * @deprecated Marked as Deprecated due to missing implementation of the
     * operations field.
     */
    @Deprecated
    public static AuditMessage getPatientRecordEvent(EventOutcomeIndicator outcome, Instant dateTime, String UID, EventActionCode actionCode, String PID, String patientName, boolean anonymized, boolean encrypted, Collection<String> operations) {
        AuditMessage msg = fillDefault(AuditFactory.ofType("PatientRecord"), outcome, dateTime, UID)
                .withObject(new ParticipantObjectIdentification()
                        .withId(PID)
                        .withTypeCodeRole(ObjectTypeCode.PERSON, ObjectTypeCodeRole.PATIENT)
                        .withIdTypeCode(ObjectIdTypeCode.PATIENT_NUMBER)
                        .withName(patientName)
                        .asAnonymized(anonymized)
                        .asEncrypted(encrypted));
        msg.getEventId().withActionCode(actionCode);
        return msg;
    }

    /**
     * Creates a DICOM Study Deleted event (DCM 110105) representing the
     * deletion of one or more studies related to the same patient.
     *
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID ID of the user attempting to delete the studies
     * @param PID ID of the patient belonging to the deleted studies
     * @param patientName Human readable name of the patient being updated
     * @param anonymized Whether the patient data is anonymized
     * @param encrypted Whether the patient data is encrypted
     * @param SIDs List of all studies being deleted
     * @return A loggable AuditMessage representing a DICOM Study Deleted event
     */
    public static AuditMessage getStudyDelete(EventOutcomeIndicator outcome, Instant dateTime, String UID, String PID, String patientName, boolean anonymized, boolean encrypted, Collection<String> SIDs) {
        AuditMessage msg = fillDefault(AuditFactory.ofType("StudyDeleted"), outcome, dateTime, UID)
                .withObject(new ParticipantObjectIdentification()
                        .withId(PID)
                        .withTypeCodeRole(ObjectTypeCode.PERSON, ObjectTypeCodeRole.PATIENT)
                        .withIdTypeCode(ObjectIdTypeCode.PATIENT_NUMBER)
                        .withName(patientName)
                        .asAnonymized(anonymized)
                        .asEncrypted(encrypted));
        SIDs.forEach(SID -> msg.withObject(new ParticipantObjectIdentification()
                .withId(SID)
                .withName(SID)
                .withTypeCodeRole(ObjectTypeCode.SYSTEM_OBJECT, ObjectTypeCodeRole.REPORT)
                .withIdTypeCode(ObjectIdTypeCode.STUDY)));
        return msg;
    }

    /**
     * Internal helper to fill static fields for all events where a real user
     * takes part as ActiveParticipant / Requestor.
     *
     * @param msg AuditMessage as created from AuditFactory
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID User ID for the ActiveParticipant requesting the event
     * @return A loggable AuditMessage
     */
    private static AuditMessage fillDefault(AuditMessage msg, EventOutcomeIndicator outcome, Instant dateTime, String UID) {
        msg.withParticipant(new ActiveParticipant()
                .withUserId(UID)
                .asRequestor())
                .getEventId()
                .withEventOutcomeIndicator(outcome)
                .withDateTime(dateTime);
        return msg;
    }

    /**
     * Internal helper to fill static fields that fit both Actor Start event
     * (DCM 110120) and Actor Stop event (DCM 110121).
     *
     * @param msg Actor Start / Stop event as created from AuditFactory
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param UID User ID for ActiveParticipant requesting the Actor Start /
     * Stop event
     * @return A loggable AuditMessage representing an Actor Start / Stop event
     */
    private static AuditMessage fillActorStartStop(AuditMessage msg, EventOutcomeIndicator outcome, Instant dateTime, String UID) {
        Selector.parse("ActiveParticipant[UserID=" + UID + "]")
                .stream(fillDefault(msg, outcome, dateTime, UID))
                .map(obj -> (ActiveParticipant) obj)
                .findFirst().ifPresent(obj -> obj.withRoleIdCode("110151", "Application Launcher"));
        return msg;
    }

    /**
     * Internal helper to static fields that fit into any event containing one
     * actor and one target user.
     *
     * @param msg AuditMessage as created from AuditFactory
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param requestorUserId User ID for ActiveParticipant
     * @param targetUserId User ID for the target user
     * @return A loggable AuditMessage containing one target user
     */
    private static AuditMessage fillTargetUser(AuditMessage msg, EventOutcomeIndicator outcome, Instant dateTime, String requestorUserId, String targetUserId) {
        return fillDefault(msg, outcome, dateTime, requestorUserId)
                .withObject(new ParticipantObjectIdentification()
                        .withId(targetUserId)
                        .withName(targetUserId)
                        .withTypeCodeRole(ObjectTypeCode.PERSON, ObjectTypeCodeRole.USER)
                        .withIdTypeCode(ObjectIdTypeCode.USER_IDENTIFIER));
    }

    /**
     * Internal helper to static fields that fit both User Activated event
     * (99DCMOTP 1) and User Deactivated event (99DCMOTP 2).
     *
     * @param msg AuditMessage as created from AuditFactory
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param requestorUserId User ID for ActiveParticipant
     * @param targetUserId User ID for the target user
     * @param studyUID UID of the study being modified
     * @return A loggable AuditMessage containing one target user
     */
    private static AuditMessage fillUserActivatedDeactivated(AuditMessage<? extends AuditMessage<?>> ofType, EventOutcomeIndicator outcome, Instant dateTime, String requestorUserId, String targetUserId, String studyUID) {
        AuditMessage msg = fillTargetUser(ofType, outcome, dateTime, requestorUserId, targetUserId);
        Selector.parse("ParticipantObject").stream(msg)
                .map(ParticipantObjectIdentification.class::cast)
                .forEach(obj -> obj.withStudy(studyUID));
        return msg;
    }

    /**
     * Internal helper to fill static fields that fit both Permission Granted
     * event (99DCMOTP 3) and Permission Revoked event (99DCMOTP 4).
     *
     * @param msg Permission Granted / Revoked event as created from
     * AuditFactory
     * @param outcome EventOutcomeIndicator
     * @param dateTime Event timestamp
     * @param requestorUserId User ID for ActiveParticipant requesting
     * permission change
     * @param targetUserId User ID for the user being modified
     * @param studyUID UID of the study being modified
     * @param permissions Permission(s) to add / remove from the user being
     * modified
     * @return A loggable AuditMessage representing an Actor Start / Stop event
     */
    private static AuditMessage fillPermission(AuditMessage msg, EventOutcomeIndicator outcome, Instant dateTime, String requestorUserId, String targetUserId, String studyUID, OtpPermissionCode... permissions) {
        msg = fillTargetUser(msg, outcome, dateTime, requestorUserId, targetUserId);
        Arrays.stream(permissions)
                .map(OtpDicomAuditFactory::createObject)
                .map(obj -> obj.withStudy(studyUID))
                .forEach(msg::withObject);
        return msg;
    }

    /**
     * Internal helper to create a ParticipantObjectIdentification from a given
     * OtpPermissionCode
     *
     * @param permission Permission to map into and ParticipantObject
     * @return A ParticipantObjectIdentification representing the given
     * OtpPermissionCode
     */
    private static ParticipantObjectIdentification createObject(OtpPermissionCode permission) {
        return new ParticipantObjectIdentification()
                .withId(String.valueOf(permission.getCodeValue()))
                .withTypeCodeRole(ObjectTypeCode.SYSTEM_OBJECT, ObjectTypeCodeRole.SECURITY_RESOURCE)
                .withChild(AuditFactory.createElementWithCodeSystemAttributes("ParticipantObjectIDTypeCode",
                        String.valueOf(OtpObjectTypeCode.PERMISSION.getCodeValue()), String.valueOf(OtpObjectTypeCode.PERMISSION.getCodeValue()),
                        OtpObjectTypeCode.PERMISSION.getDescription(), OtpObjectTypeCode.PERMISSION.getDescription()))
                .withName(permission.getDescription());
    }

    /**
     * Generates a UID from the UID prefix, the supplied type and identifier for
     * usage in logger methods.
     *
     * @param type Type of the UID to be generated
     * @param identifier Locally or domain-wise unique identifier for the object
     * in question
     * @return
     */
    public static String generateUID(UniqueIdentifierType type, String identifier) {
        return UID_PREFIX + UID_SEPARATOR + type.getIntVal() + UID_SEPARATOR + identifier;
    }
}
